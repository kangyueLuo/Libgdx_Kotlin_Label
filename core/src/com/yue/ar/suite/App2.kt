package com.yue.ar.suite

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.utils.Layout
import com.badlogic.gdx.utils.viewport.StretchViewport


class App2 : ApplicationAdapter(){

    internal var batch: SpriteBatch ? = null
    internal var img: Texture? = null

    internal var font_ch: BitmapFont? = null
    internal var generator_ch: FreeTypeFontGenerator ? = null
    internal var parameter_ch: FreeTypeFontGenerator.FreeTypeFontParameter ? = null

    internal var strShowText = "Brad 恭賀新禧 !@#$%^&()_+=-`~*/.<>;':{}[]| 1234567890 " // 造字檔

    private var stage : Stage? = null
    private var label :Label? = null

    private val width : Float = 480.0f
    private val height : Float = 800.0f


    override fun create() {
        batch = SpriteBatch()

        // 使用伸展视口（StretchViewport）创建舞台
        stage = Stage(StretchViewport(width,height))

        /*
         * 第 1 步: 创建 BitmapFont
         */
        // 读取 bitmapfont.fnt 文件创建位图字体
        generator_ch = FreeTypeFontGenerator(Gdx.files.internal("font/DroidSansFallback.ttf"))
        parameter_ch = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter_ch!!.size = 24
        parameter_ch!!.characters = strShowText
        font_ch = generator_ch!!.generateFont(parameter_ch)
        generator_ch!!.dispose()

        /*
        * 第 2 步: 创建 LabelStyle
        */
        // 要创建 Label 首先要创建一个 Label 的样式, 用于指明 Label 所使用的位图字体, 背景图片, 颜色等
        val style : com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle = com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle()
        style.font = font_ch
        style.fontColor = Color(0f,1f,1f,1f)


        /*
         * 第 3 步: 创建 Label
         */
        // 根据 Label 的样式创建 Label, 第一个参数表示显示的文本（要显示的文本字符必须在 BitmapFont 中存在）
         label = Label(strShowText, style)

        // 也可以通过方法设置或获取文本
        // label.setText("Hello");
        // String text = label.getText().toString();

        // 设置 Label 的显示位置
        label!!.setPosition(50f, 400f);


        // 可以通过设置字体的缩放比来控制字体显示的大小
        label!!.setFontScale(1.5f);
        label!!.setFontScale(1.5f)
        /*
         * 第 4 步: 添加到舞台
         */
        // 添加 label 到舞台
       
        stage!!.addActor(label)

    }

    override fun render() {
        super.render()
    }

    override fun dispose() {
        super.dispose()
    }

}
